const fs = require('fs');

// add url-route in /controllers:

function addMapping(router, mapping) {
    for (var url in mapping) {
        if (url.startsWith('GET ')) {
            var path = url.substring(4);
            if (typeof mapping[url] == 'object' && mapping[url].middleWare){
                router.get(path, mapping[url].middleWare,mapping[url].handler);
            }else{
                router.get(path, mapping[url]);
            }
            // console.log(`register URL mapping: GET ${path}`);
        } else if (url.startsWith('POST ')) {
            var path = url.substring(5);
            if (typeof mapping[url] == 'object' && mapping[url].middleWare){
                router.post(path, mapping[url].middleWare,mapping[url].handler);
            }else{
                router.post(path, mapping[url]);
            }
            // console.log(`register URL mapping: POST ${path}`);
        } else if (url.startsWith('PUT ')) {
            var path = url.substring(4);
            if (typeof mapping[url] == 'object' && mapping[url].middleWare){
                router.put(path, mapping[url].middleWare,mapping[url].handler);
            }else{
                router.put(path, mapping[url]);
            }
            // console.log(`register URL mapping: PUT ${path}`);
        } else if (url.startsWith('DELETE ')) {
            var path = url.substring(7);
            if (typeof mapping[url] == 'object' && mapping[url].middleWare){
                router.del(path, mapping[url].middleWare,mapping[url].handler);
            }else{
                router.del(path, mapping[url]);
            }
            // console.log(`register URL mapping: DELETE ${path}`);
        } else {
            console.log(`invalid URL: ${url}`);
        }
    }
}

function addControllers(router, dir) {
    fs.readdirSync(__dirname + '/' + dir).filter((f) => {
        return f.endsWith('.js');
    }).forEach((f) => {
        // console.log(`process controller: ${f}...`);
        let mapping = require(__dirname + '/' + dir + '/' + f);//对应module.exports
        addMapping(router, mapping);
    });
}

module.exports = function (dir) {
    let controllers_dir = dir || 'controller',
        router = require('koa-router')();
    addControllers(router, controllers_dir);
    return router.routes();
};