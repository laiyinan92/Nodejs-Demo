module.exports = {
    publicDer: '-----BEGIN PUBLIC KEY-----\n' +
        'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIEoZ4Lum3mkDhzUF8+2yfiwEeBb9cib\n' +
        'S6y8QlOiSkzlk/rWfwmUgW1gsCTJsUd5mcsQeef/ceVD0kKWgpGKfi0CAwEAAQ==\n' +
        '-----END PUBLIC KEY-----',
    privateDer: '-----BEGIN RSA PRIVATE KEY-----\n' +
        'MIIBOQIBAAJBAIEoZ4Lum3mkDhzUF8+2yfiwEeBb9cibS6y8QlOiSkzlk/rWfwmU\n' +
        'gW1gsCTJsUd5mcsQeef/ceVD0kKWgpGKfi0CAwEAAQJAdd+NEIZ46DvcEV1TWeNF\n' +
        'UqxFtE/Y8TshhkTN94v+aUwy2uB0a4+viVTAWEji5D2npzkUj5phhRSfTxFcMLBi\n' +
        'oQIhAMnRZtXDCG/iSb46OhaMe3LXks/B4yCUnx8gcvEVxFk1AiEAo9UyfgquJHLb\n' +
        'xN8u7qWBDFIQ8gxU3ueXPKxJVLjnqBkCIHBrHpdQLrD0IhO+p4DL/C/6eSMVY4lQ\n' +
        'gFp8XsqAT4FJAiBnpr/OPIWWa9CdCrLPq4gsulMcQC8F47GsYjf20RFHKQIgDT9+\n' +
        'YL6m7JNYd5wmTuj6zzkPRLcXfnU4XHzTeNkZB3w=\n' +
        '-----END RSA PRIVATE KEY-----',
    parseUrl: (url) => {
        let paramsString = url.split('?')[1];
        if (!paramsString) return {};
        paramsString = paramsString.split('&');
        let params = {};
        paramsString.forEach(item => {
            let i = item.split('=');
            params[i[0]] = i[1];
        });
        return params;
    },
    decode: (json) => {
        let obj = {};
        for (let item in json) {
            let temp = decodeURIComponent(json[item]);
            obj[item] = temp;
        }
        return obj;
    },
    findRoot: (data, item) => {
        let depth = 0;
        while (item.replyId != 0) {
            depth++;
            item = data.find(i => i.id == item.replyId);
            if (!item){
                break;
            }
            // console.log(depth, item.id)
        }
        return item;
    }
};