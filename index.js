const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const controller = require('./controller');
const app = new Koa();

app.use(async (ctx,next)=>{
    ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    console.info(`processing ${ctx.request.method} ${ctx.request.url}`);
    if (ctx.request.method == 'OPTIONS'){
        ctx.response.status = 200;
    }
    await next();
});
app.use(bodyParser());
app.use(controller());
app.listen(3001);
console.info('app is listening at port 3001');