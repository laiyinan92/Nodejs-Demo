const query = require('../queryEngine/index.js');
const tool = require('../common/index.js');
const sequelize = require('sequelize');

module.exports = {
    'POST /createComment': async (ctx, next) => {
        ctx.request.type = 'application/x-www-form-urlencoded';
        await query.createComment(tool.decode(ctx.request.body)).then(() => {
            ctx.response.body = {
                errorCode: 0,
                errorMsg: '成功'
            };
        }).catch((err) => {
            ctx.response.status = 492;
            ctx.response.message = 'error:can not create comment, please try again later';
        });
    },
    'GET /comment': async (ctx, next) => {
        ctx.response.type = 'application/json';
        let result = await query.comment({where: {...tool.parseUrl(ctx.request.url), flag: 0}});
        let responseBody = [];
        result.forEach(async item => {
            if (item.replyId == 0) {
                item.dataValues.children = [];
                responseBody.push(item)
            }
        });
        result.forEach(async item => {
            if (item.replyId != 0 && result.some(i => i.id == item.replyId)) {
                let father = result.filter(i => i.id == item.replyId)[0];
                let root = tool.findRoot(result, item);
                if (root) {
                    let index = responseBody.indexOf(root);
                    if (index >= 0) {
                        item.dataValues.replyTitle = father.title;
                        item.dataValues.replyUser = father.userNickname;
                        responseBody[index].dataValues.children.push(item);
                    }
                }
            }
        });
        ctx.response.body = responseBody;
    },
    'DELETE /deleteComment': async (ctx, next) => {
        ctx.response.type = 'application/x-www-form-urlencoded';
        await query.deleteComment({where: {id: Number(tool.parseUrl(ctx.request.url).id)}}).then((res) => {
            ctx.response.body = {
                errorCode: 0,
                errorMsg: '成功'
            };
        }).catch(err => {
            ctx.response.status = 493;
            ctx.response.message = err.name;
        })
    },
    'GET /unreadMessges': async (ctx, next) => {
        ctx.response.type = 'application/json';
        ctx.response.body = await query.comment({where: {...tool.parseUrl(ctx.request.url), flag: 0}})
    },
    'GET /readMessage': async (ctx, next) => {
        await query.readComment({where: {id: Number(tool.parseUrl(ctx.request.url).id)}}).then((res) => {
            ctx.response.body = {
                errorCode: 0,
                errorMsg: '成功'
            };
        }).catch(err => {
            ctx.response.status = 494;
            ctx.response.message = err.name;
        })
    },
    'GET /readAllMessage': async (ctx, next) => {
        await query.readComment({
            where:
                {
                    id:{
                        [sequelize.Op.or]:tool.parseUrl(ctx.request.url).ids.split(',')
                    }
                }
        }).then((res) => {
            ctx.response.body = {
                errorCode: 0,
                errorMsg: '成功'
            };
        }).catch(err => {
            console.log(err)
            ctx.response.status = 495;
            ctx.response.message = err.name;
        })
    }
};