const query = require('../queryEngine/index.js');
const tool = require('../common/index.js')

module.exports = {
    'GET /menus': async (ctx, next) => {
        ctx.response.type = 'application/json';
        ctx.response.body = await query.menu({where: {...tool.parseUrl(ctx.request.url), flag: 0}})
    },
    'GET /addMenuItem': async (ctx, next) => {
        ctx.response.type = 'application/json';
        await query.addMenuItem(tool.parseUrl(decodeURIComponent(ctx.request.url))).then(() => {
            ctx.response.body = {
                errorCode: 0,
                errorMsg: '成功'
            };
        }).catch((err) => {
            ctx.response.status = 483;
            ctx.response.message = err.name;
        });
    },
    'DELETE /deleteMenuItem': async (ctx, next) => {
        ctx.response.type = 'application/x-www-form-urlencoded';
        await query.deleteMenuItem( {where: {id: Number(tool.parseUrl(ctx.request.url).id)}}).then((res) => {
            ctx.response.body = {
                errorCode: 0,
                errorMsg: '成功'
            };
        }).catch(err => {
            ctx.response.status = 486;
            ctx.response.message = err.name;
        })
    }
};