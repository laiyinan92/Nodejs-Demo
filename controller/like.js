const query = require('../queryEngine/index.js');
const tool = require('../common/index.js');
const sequelize = require('sequelize');

module.exports = {
    'GET /createLike':async (ctx,next)=>{
        ctx.request.type = 'application/x-www-form-urlencoded';
        await query.createLike(tool.parseUrl(ctx.request.url)).then((res) => {
            ctx.response.body = res;
        }).catch((err) => {
            ctx.response.status = 523;
            ctx.response.message = 'error:can not like or unlike now';
        });
    },
    'GET /updateLike':async(ctx,next)=>{
        ctx.response.type = 'application/json';
        let requestBody = tool.parseUrl(decodeURIComponent(ctx.request.url));
        await query.updateLike({
            status: requestBody.status,
        }, {where: {id: requestBody.id}}).then((res) => {
            ctx.response.body = res;
        }).catch(err=>{
            ctx.response.status = 524;
            ctx.response.message = err.name;
        })
    }
};